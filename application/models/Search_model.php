<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_model extends CI_Model {

	public function get_all_restaurants($keyword) {
      if($keyword != FALSE) {
	    $this->db->select('*');
	    $this->db->from('restaurant');
	    $this->db->like('restaurantName', $keyword);
		$this->db->or_like('description', $keyword); 
		$this->db->or_like('address', $keyword); 
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
    }

    public function get_all_deals($keyword) {
      if($keyword != FALSE) {
	    $this->db->select('*');
	    $this->db->from('item');
	    $this->db->like('restaurantName', $keyword);
		$this->db->or_like('description', $keyword); 
		$this->db->or_like('itemName', $keyword); 
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
    }
}