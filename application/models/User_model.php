<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function get_user($email) {
	  $email = str_replace("%40","@",$email);
	  if($email != FALSE) {
		//For selecting one or more columns
	    $this->db->select('*');
	    //For determine one or more tables to select from 
	    $this->db->from('customer');
	    //For joining with another table, table name as first argument and condition string as second argument
	    //$this->db->join('table2', 'comments.id = blogs.id');
	    //assign where condition
	    $this->db->where('email', $email); 
	    //function without any argument. It actually runs the query that was built on last few statements.
	    $query = $this->db->get();
	    //returns result objects array
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
	}

	public function get_admin_user($email) {
	  $email = str_replace("%40","@",$email);
	  if($email != FALSE) {
	    $this->db->select('*');
	    $this->db->from('owner');
	    $this->db->where('email', $email); 
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
	}

	public function authenticate($email, $password) {
	  $email = str_replace("%40","@",$email);
	  if($email != FALSE) {
	    $this->db->select('*');
	    $this->db->from('customer');
	    $this->db->where('email', $email); 
	    $this->db->where('password',$password);
	    $query = $this->db->get();
	    if ($query->num_rows() == 1) {
    		return TRUE;
		} else {
		    return FALSE;
		}
	  }
	  else {
	    return FALSE;
	  }
	}

	public function authenticateAdmin($email, $password) {
	  $email = str_replace("%40","@",$email);
	  if($email != FALSE) {
	    $this->db->select('*');
	    $this->db->from('owner');
	    $this->db->where('email', $email); 
	    $this->db->where('password',$password);
	    $query = $this->db->get();
	    if ($query->num_rows() == 1) {
    		return TRUE;
		} else {
		    return FALSE;
		}
	  }
	  else {
	    return FALSE;
	  }
	}

	public function insertUser($data) {
		$this->db->insert('customer', $data);
		return $this->db->affected_rows() > 0;
	}

	public function insertAdminUser($data) {
		$this->db->insert('owner', $data);
		return $this->db->affected_rows() > 0;
	}

	public function get_history($email) {
	  $email = str_replace("%40","@",$email);
	  if($email != FALSE) {
	    $this->db->select('*');
	    $this->db->from('purchase');
	    $this->db->where('email', $email); 
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
	}

	public function get_last($email) {
	  $email = str_replace("%40","@",$email);
	  if($email != FALSE) {
	    $this->db->select('*');
	    $this->db->from('purchase');
	    $this->db->where('email', $email); 
	    $this->db->where('issued', false);
	    $this->db->order_by("dateTime", "desc");
	    $this->db->limit(1);
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
	}
}