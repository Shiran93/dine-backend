<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item_model extends CI_Model {

	public function get_all_items() {
	    $this->db->select('*');
	    $this->db->from('item');
	    $this->db->where('enabled', TRUE); 
	    $this->db->join('restaurant', 'item.restaurantName = restaurant.restaurantName');
	    $query = $this->db->get();
	    return $query->result();
    }

    public function get_popular_items() {
	   $SQL = "SELECT * FROM item Where itemID IN (SELECT i.itemID FROM purchase p, item i WHERE p.itemID = i.itemID AND i.enabled = true GROUP BY i.itemID ORDER BY COUNT(*) DESC) ORDER BY RAND() LIMIT 5";

		$query = $this->db->query($SQL);

		return $query->result_array();
    }

    public function get_rated_items() {
    	$SQL = "SELECT * FROM item Where itemID IN (SELECT i.itemID FROM purchase p, item i WHERE p.itemID = i.itemID  AND i.enabled = true  GROUP BY i.itemID ORDER BY SUM(p.rating) DESC) ORDER BY RAND() LIMIT 5";
    	$query = $this->db->query($SQL);

		return $query->result_array();
    }

    public function get_item_images($id) {
	  if($id != FALSE) {
	    $this->db->select('link');
	    $this->db->from('image');
	    $this->db->where('itemID', $id); 
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
	}

	public function get_item_by_id($id) {
	  if($id != FALSE) {
	    $this->db->select('*');
	    $this->db->from('item');
	    $this->db->where('itemID', $id); 
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
	}

	public function get_item_by_name($id) {
	  if($id != FALSE) {
	    $this->db->select('*');
	    $this->db->from('item');
	    $this->db->where('itemName', $id); 
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
	}

	public function get_restaurant_images($id) {
	  if($id != FALSE) {
	    $this->db->select('link');
	    $this->db->from('image');
	    $this->db->where('restaurantName', $id); 
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
	}

	public function get_item_rating($id) {
	  if($id != FALSE) {
	    $this->db->select_avg('rating');
	    $this->db->from('purchase');
	    $this->db->where('itemID', $id); 
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
	}

	public function get_item_feedback_count($id) {
	  if($id != FALSE) {
	    $this->db->select('count(*) as count');
	    $this->db->from('purchase');
	    $this->db->where('itemID', $id); 
	    $this->db->where('rating IS NOT NULL');
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
	}

	public function get_item_feedbacks($id) {
	  if($id != FALSE) {
	    $this->db->select('*');
	    $this->db->from('purchase');
	    $this->db->where('itemID', $id);
	    $this->db->where('rating IS NOT NULL'); 
	   	$this->db->join('customer', 'purchase.email = customer.email'); 
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
	}

	public function get_item_price($id) {
		$this->db->select('discountPrice');
	    $this->db->from('item');
	    $this->db->where('itemID', $id);
	    $query = $this->db->get();
	    return $query->row('discountPrice');
	}

	public function get_sales_list($restaurant) {
		$SQL = "SELECT i.itemID, COUNT(*) as total FROM purchase p, item i WHERE i.restaurantName = ? AND p.itemID = i.itemID GROUP BY i.itemID";

		$query = $this->db->query($SQL,$restaurant);

		return $query->result_array();
	}

	public function insert_order($data) {
		$this->db->insert('purchase', $data);
		return $this->db->affected_rows() > 0;
	}

	public function insert_item($data) {
		$this->db->insert('item', $data);
		return $this->db->affected_rows() > 0;
	}

	public function insert_item_image($data) {
		$this->db->insert('image', $data);
		return $this->db->affected_rows() > 0;
	}

	public function update_item($id,$data) {
		$this->db->where('itemID', $id);
		$this->db->update('item', $data);
		return $this->db->affected_rows() > 0;
	}

	public function delete_item($id,$data) {
		$this->db->where('itemID', $id);
		$this->db->update('item', $data);
		return $this->db->affected_rows() > 0;
	}
	
	public function add_feedback_rating($id,$data) {
		$this->db->where('couponCode', $id);
		$this->db->update('purchase', $data);
		return $this->db->affected_rows() > 0;
	}

	public function is_eligible_to_feedback($email,$itemID) {
		$this->db->select('*');
	    $this->db->from('purchase');
	    $this->db->where('itemID', $itemID);
	    $this->db->where('email', $email);
	    $this->db->where('rating IS NULL');
	   	$this->db->limit(1);

	    $query = $this->db->get();
	    $rowcount = $query->num_rows();

	    if($rowcount > 0)
	    	return $query->result();
	    else
	    	return FALSE;
	}

	public function get_items_by_restaurant_name($name) {
      if($name != FALSE) {
	    $this->db->select('*');
	    $this->db->from('item');
	    $this->db->where('restaurantName', $name);
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
    }
}