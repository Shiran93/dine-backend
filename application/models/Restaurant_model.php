<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurant_model extends CI_Model {

	public function get_all_restaurants() {
	    $this->db->select('*');
	    $this->db->from('restaurant');
	    $query = $this->db->get();
	    return $query->result();
    }

    public function get_restaurant_by_name($name) {
      if($name != FALSE) {
	    $this->db->select('*');
	    $this->db->from('restaurant');
	    $this->db->where('restaurantName', $name); 
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
    }

    public function get_restaurant_by_item($id) {
      if($id != FALSE) {
	    $this->db->select('*');
	    $this->db->from('item');
	    $this->db->where('itemID', $id);
	    $this->db->join('restaurant', 'item.restaurantName = restaurant.restaurantName'); 
	    $query = $this->db->get();
	    return $query->result();
	  }
	  else {
	    return FALSE;
	  }
    }

    public function get_un_collected_orders($id) {
      	$SQL = "SELECT * FROM item i, purchase p
				WHERE i.itemID = p.itemID AND i.restaurantName = ?
				AND p.issued = false";
    	$query = $this->db->query($SQL,$id);

		return $query->result_array();
    }

    public function insert_restaurant($data) {
		$this->db->insert('restaurant', $data);
		return $this->db->affected_rows() > 0;
	}

	public function insert_restaurant_owner($data,$owner) {
		$this->db->where('email', $owner);
		$this->db->update('owner', $data);
		return $this->db->affected_rows() > 0;
	}

	public function update_restaurant($name,$data) {
		$this->db->where('restaurantName', $name);
		$this->db->update('restaurant', $data);
		return $this->db->affected_rows() > 0;
	}

	public function update_issued($name,$data) {
		$this->db->where('couponCode', $name);
		$this->db->update('purchase', $data);
		return $this->db->affected_rows() > 0;
	}

	public function insert_restaurant_image($data) {
		$this->db->insert('image', $data);
		return $this->db->affected_rows() > 0;
	}
}