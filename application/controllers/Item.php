<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/controllers/REST_Controller.php';

class Item extends REST_Controller {

	public function getAllItems_get() {
		$this->load->model('item_model');
        $items = $this->item_model->get_all_items();
		$this->response($items,REST_Controller::HTTP_OK);
	}

	public function getPopularItems_get() {
		$this->load->model('item_model');
        $items = $this->item_model->get_popular_items();
		$this->response($items,REST_Controller::HTTP_OK);
	}

	public function getRatedItems_get() {
		$this->load->model('item_model');
        $items = $this->item_model->get_rated_items();
		$this->response($items,REST_Controller::HTTP_OK);
	}

	public function getItemPrice_get($id) {
		$this->load->model('item_model');
        $price = $this->item_model->get_item_price($id);
		$this->response($price,REST_Controller::HTTP_OK);
	}

	public function getItemByName_get($id) {
		$this->load->model('item_model');
        $price = $this->item_model->get_item_by_name(urldecode($id));
		$this->response($price,REST_Controller::HTTP_OK);
	}


	public function getItemImages_get($id) {
		$this->load->model('item_model');
        $images = $this->item_model->get_item_images(urldecode($id));
		$this->response($images,REST_Controller::HTTP_OK);
	}

	public function getRestaurantImages_get($id) {
		$this->load->model('item_model');
        $images = $this->item_model->get_restaurant_images(urldecode($id));
		$this->response($images,REST_Controller::HTTP_OK);
	}

	public function getItemRating_get($id) {
		$this->load->model('item_model');
        $rating = $this->item_model->get_item_rating(urldecode($id));
		$this->response($rating,REST_Controller::HTTP_OK);
	}

	public function getItemByID_get($id) {
		$this->load->model('item_model');
        $item = $this->item_model->get_item_by_id(urldecode($id));
		$this->response($item,REST_Controller::HTTP_OK);
	}

	public function getItemFeedbackCount_get($id) {
		$this->load->model('item_model');
        $feedbackCount = $this->item_model->get_item_feedback_count(urldecode($id));
		$this->response($feedbackCount,REST_Controller::HTTP_OK);
	}

	public function getItemFeedbacks_get($id) {
		$this->load->model('item_model');
        $feedbacks = $this->item_model->get_item_feedbacks(urldecode($id));
		$this->response($feedbacks,REST_Controller::HTTP_OK);
	}

	public function order_get($userID, $itemID, $qty, $token) {
		$this->load->model('item_model');
		$data = array(
			'email' => urldecode($userID),
			'itemID' => $itemID,
			'quantity' => $qty,
			'couponCode' => $this->gen_uuid(),
			'issued' => FALSE,
			'totalPrice' => $this->item_model->get_item_price($itemID)*$qty,
			);

		$inserted = $this->item_model->insert_order($data);

		if($inserted) {

			\Stripe\Stripe::setApiKey("sk_test_TxluiOL2QTJkLewcssPb9QCu");

			// Create a charge: this will charge the user's card
			try {
			  	$charge = \Stripe\Charge::create(array(
			    "amount" => $this->item_model->get_item_price($itemID)*$qty, // Amount in cents
			    "currency" => "aud",
			    "source" => urldecode($token),
			    "description" => "Example charge"
			    ));
			} catch(\Stripe\Error\Card $e) {
			  // The card has been declined
			}

			$this->set_response(REST_Controller::HTTP_CREATED);
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function insertItem_get($itemName, $des, $disPrice, $actPrice, $restName) {
		$this->load->model('item_model');
		$data = array(
			'itemName' => urldecode($itemName),
			'description' => urldecode($des),
			'discountPrice' => $disPrice,
			'actualPrice' => $actPrice,
			'restaurantName' => urldecode($restName),
			);

		$inserted = $this->item_model->insert_item($data);

		if($inserted) {
			$this->set_response(REST_Controller::HTTP_CREATED);
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function insertItemImage_get($itemID, $link) {
		$this->load->model('item_model');
		$data = array(
			'itemID' => urldecode($itemID),
			'link' => urldecode($link)
			);

		$inserted = $this->item_model->insert_item_image($data);

		if($inserted) {
			$this->set_response(REST_Controller::HTTP_CREATED);
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function updateItem_get($id, $itemName, $des, $disPrice, $actPrice, $restName) {
		$this->load->model('item_model');
		$data = array(
			'itemName' => urldecode($itemName),
			'description' => urldecode($des),
			'discountPrice' => $disPrice,
			'actualPrice' => $actPrice,
			'restaurantName' => urldecode($restName),
			);

		$updated = $this->item_model->update_item($id,$data);

		if($updated) {
			$this->set_response(REST_Controller::HTTP_CREATED);
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function deleteItem_get($id) {
		$this->load->model('item_model');
		$data = array(
			'enabled' => FALSE,
			);

		$deleted = $this->item_model->delete_item($id,$data);

		if($deleted) {
			$this->set_response(REST_Controller::HTTP_CREATED);
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function enableItem_get($id) {
		$this->load->model('item_model');
		$data = array(
			'enabled' => TRUE,
			);

		$deleted = $this->item_model->delete_item($id,$data);

		if($deleted) {
			$this->set_response(REST_Controller::HTTP_CREATED);
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function getItemsByRestaurantName_get($name) {
		$this->load->model('item_model');
        $items = $this->item_model->get_items_by_restaurant_name(urldecode($name));
		$this->response($items,REST_Controller::HTTP_OK);
	}

	public function addFeedBackRating_get($couponCode,$feedback,$rating) {
		$this->load->model('item_model');
		$data = array(
			'feedback' => urldecode($feedback),
			'rating' => urldecode($rating),
			);

		$updated = $this->item_model->add_feedback_rating(urldecode($couponCode),$data);

		if($updated) {
			$this->set_response(REST_Controller::HTTP_CREATED);
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function isEligibleToFeedback_get($email, $itemID) {
		$this->load->model('item_model');
		
		$eligible = $this->item_model->is_eligible_to_feedback(urldecode($email),$itemID);

		if($eligible != FALSE) {
			$this->response($eligible,REST_Controller::HTTP_OK);
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function getSalesList_get($restaurant) {
		$this->load->model('item_model');
		$items = $this->item_model->get_sales_list(urldecode($restaurant));
		$this->response($items,REST_Controller::HTTP_OK);
	}

	function gen_uuid() {
    	return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}
}
