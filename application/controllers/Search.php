<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/controllers/REST_Controller.php';

class Search extends REST_Controller {

	public function searchRestaurant_get($keyword) {
		$this->load->model('search_model');
        $restaurants = $this->search_model->get_all_restaurants(urldecode($keyword));
		$this->response($restaurants,REST_Controller::HTTP_OK);
	}

	public function searchDeal_get($keyword) {
		$this->load->model('search_model');
        $deals = $this->search_model->get_all_deals(urldecode($keyword));
		$this->response($deals,REST_Controller::HTTP_OK);
	}
}