<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/controllers/REST_Controller.php';

class User extends REST_Controller {

	public function getUserByEmail_get($email) {
		$this->load->model('user_model');
        $user = $this->user_model->get_user($email);
		$this->response($user);
	}

	public function login_get($email,$password) {
		$this->load->model('user_model');
        $authenticated = $this->user_model->authenticate($email,$password);

        if($authenticated) {
        	$user = $this->user_model->get_user($email);
        	$user['isAdmin'] = FALSE;
        	$this->response($user,REST_Controller::HTTP_OK);
        }
        else if ($this->user_model->authenticateAdmin($email,$password)) {
        	$user = $this->user_model->get_admin_user($email);
        	$user['isAdmin'] = TRUE;
        	$this->response($user,REST_Controller::HTTP_OK);
        }
        else {
        	$this->response(REST_Controller::HTTP_UNAUTHORIZED);
        }
	}

	public function register_get($fName, $lName, $email, $password, $url) {
		$this->load->model('user_model');
		$data = array(
			'firstName' => urldecode($fName),
			'lastName' => urldecode($lName),
			'email' => urldecode($email),
			'password' => urldecode($password),
			'profilePicture' => urldecode($url)
			);

		$user = $this->user_model->get_admin_user($email);
		if(empty($user)) {
			$inserted = $this->user_model->insertUser($data);
			
			if($inserted) {
				$this->set_response(REST_Controller::HTTP_CREATED);
			}
			else {
				$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
			}
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function register_admin_get($fName, $lName, $email, $password) {
		$this->load->model('user_model');
		$data = array(
			'firstName' => $fName,
			'lastName' => $lName,
			'email' => str_replace('%40','@',$email),
			'password' => $password,
			);

		$user = $this->user_model->get_user($email);
		if(empty($user)) {
			$inserted = $this->user_model->insertAdminUser($data);

			if($inserted) {
				$this->set_response(REST_Controller::HTTP_CREATED);
			}
			else {
				$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
			}
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function getPurchaseHistory_get($email) {
		$this->load->model('user_model');
        $history = $this->user_model->get_history(urldecode($email));
		$this->response($history,REST_Controller::HTTP_OK);
	}

	public function getLastPurchase_get($email) {
		$this->load->model('user_model');
        $history = $this->user_model->get_last(urldecode($email));
		$this->response($history,REST_Controller::HTTP_OK);
	}
}