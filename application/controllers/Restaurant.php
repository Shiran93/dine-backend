<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/controllers/REST_Controller.php';

class Restaurant extends REST_Controller {

	public function getAllRestaurants_get() {
		$this->load->model('restaurant_model');
        $restaurants = $this->restaurant_model->get_all_restaurants();
		$this->response($restaurants,REST_Controller::HTTP_OK);
	}

	public function getRestaurantByname_get($name) {
		$this->load->model('restaurant_model');
        $restaurant = $this->restaurant_model->get_restaurant_by_name(urldecode($name));
		$this->response($restaurant,REST_Controller::HTTP_OK);
	}

	public function getRestaurantByItemID_get($id) {
		$this->load->model('restaurant_model');
        $restaurant = $this->restaurant_model->get_restaurant_by_item(urldecode($id));
		$this->response($restaurant,REST_Controller::HTTP_OK);
	}

	public function getUnCollectedOrders_get($res) {
		$this->load->model('restaurant_model');
        $restaurant = $this->restaurant_model->get_un_collected_orders(urldecode($res));
		$this->response($restaurant,REST_Controller::HTTP_OK);
	}

	public function updateIssued_get($couponCode) {
		$this->load->model('restaurant_model');
		$data = array(
			'issued' => true,
			);

		$updated = $this->restaurant_model->update_issued(urldecode($couponCode),$data);

		if($updated) {
			$this->set_response(REST_Controller::HTTP_CREATED);
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function insertRestaurant_get($owner, $name,$address,$desc,$paypal,$stripe,$sun,$mon,$tue,$wed,$thu,$fri,$sat) {
		$this->load->model('restaurant_model');
		$data = array(
			'restaurantName' => urldecode($name),
			'address' => urldecode($address),
			'description' => urldecode($desc),
			'paypal' => urldecode($paypal),
			'stripe' => urldecode($stripe),
			'sunday' => urldecode($sun),
			'monday' => urldecode($mon),
			'tuesday' => urldecode($tue),
			'wednesday' => urldecode($wed),
			'thursday' => urldecode($thu),
			'friday' => urldecode($fri),
			'saturday' => urldecode($sat),
			);

		$data2 = array(
			'restaurantName' =>urldecode($name)
			);

		$inserted = $this->restaurant_model->insert_restaurant($data);
		$inserted2 = $this->restaurant_model->insert_restaurant_owner($data2,$owner);


		if($inserted && $inserted2) {
			$this->set_response(REST_Controller::HTTP_CREATED);
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function updateRestaurant_get($name,$address,$desc,$paypal,$stripe,$sun,$mon,$tue,$wed,$thu,$fri,$sat) {
		$this->load->model('restaurant_model');
		$data = array(
			'address' => urldecode($address),
			'description' => urldecode($desc),
			'paypal' => urldecode($paypal),
			'stripe' => urldecode($stripe),
			'sunday' => urldecode($sun),
			'monday' => urldecode($mon),
			'tuesday' => urldecode($tue),
			'wednesday' => urldecode($wed),
			'thursday' => urldecode($thu),
			'friday' => urldecode($fri),
			'saturday' => urldecode($sat),
			);

		$updated = $this->restaurant_model->update_restaurant(urldecode($name),$data);


		if($updated) {
			$this->set_response(REST_Controller::HTTP_CREATED);
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function insertRestaurantImage_get($itemID, $link) {
		$this->load->model('restaurant_model');
		$data = array(
			'restaurantName' => urldecode($itemID),
			'link' => urldecode($link)
			);

		$inserted = $this->restaurant_model->insert_restaurant_image($data);

		if($inserted) {
			$this->set_response(REST_Controller::HTTP_CREATED);
		}
		else {
			$this->set_response(REST_Controller::HTTP_NOT_ACCEPTABLE);
		}
	}

}